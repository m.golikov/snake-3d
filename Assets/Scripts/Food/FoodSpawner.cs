using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Experimental.GraphView.GraphView;

public class FoodSpawner : MonoBehaviour
{
    public Food foodPrefab;
    public int foodCount;
    public float spawnSphereRadius, groundDistance;
    public LayerMask groundLayerMask;

    private void Start()
    {
        for (var i = 0; i < foodCount; i++)
        {
            var food = Instantiate(foodPrefab, transform);
            food.onDestroy.AddListener(() => PlaceFoodRandom(food));
            PlaceFoodRandom(food);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, spawnSphereRadius);
    }

    public void PlaceFoodRandom(Food food)
    {
        var point = transform.position + Random.onUnitSphere * spawnSphereRadius;

        if (Physics.Raycast(point, transform.position - point, out var hitInfo))
        {
            if (groundLayerMask == (groundLayerMask | (1 << hitInfo.collider.gameObject.layer)))
            {
                food.transform.position = hitInfo.point + hitInfo.normal * groundDistance;
                return;
            }
            PlaceFoodRandom(food);
        }
    }
}
