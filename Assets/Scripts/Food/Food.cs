using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Food : MonoBehaviour
{
    public int foodValue;
    public UnityEvent onDestroy;

    public void Destroy()
    {
        onDestroy.Invoke();
    }
}
