using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CinemachineFollowObjectRelatively : CinemachineExtension
{
    public float smoothFactor = 5;
    public bool setOffsetsOnAwake;
    public Vector3 offset;
    public Quaternion deltaRotation;
    
    protected override void Awake()
    {
        base.Awake();
        
        if (setOffsetsOnAwake)
        {
            offset = VirtualCamera.Follow.InverseTransformVector(VirtualCamera.transform.position - VirtualCamera.Follow.position);
            deltaRotation = Quaternion.Inverse(VirtualCamera.LookAt.rotation) * VirtualCamera.transform.rotation;
        }
    }

    protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        state.RawPosition = Vector3.Lerp(state.CorrectedPosition, 
            vcam.Follow.position + vcam.Follow.TransformVector(offset), smoothFactor * deltaTime);
        state.RawOrientation = Quaternion.Slerp(state.CorrectedOrientation, 
            vcam.LookAt.rotation * deltaRotation, smoothFactor * deltaTime);
    }
}
