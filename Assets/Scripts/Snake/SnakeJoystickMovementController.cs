using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeJoystickMovementController : MonoBehaviour
{
    public Joystick joystick;
    public SnakeMovement snakeMovement;
    public Transform mainCameraTransform;

    private Vector3 _moveDirection = Vector2.up;

    private void Update()
    {
        var relativeMoveDirection = snakeMovement.transform
            .InverseTransformDirection(mainCameraTransform.TransformDirection(_moveDirection));

        snakeMovement.moveDirection = new Vector2(relativeMoveDirection.x, relativeMoveDirection.z);

        if (joystick.Direction == Vector2.zero)
        {
            return;
        }

        _moveDirection = joystick.Direction;
    }
}
