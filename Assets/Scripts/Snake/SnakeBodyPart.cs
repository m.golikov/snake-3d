using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class SnakeBodyPart : MonoBehaviour
{
    public float positionSaveDistance, smoothFactor;
    public Transform art;

    public class SnakeBodyPartTransformData
    {
        public Vector3 position;
        public Quaternion rotation;
    }
    public List<SnakeBodyPartTransformData> transformHistory { get; private set; } = new List<SnakeBodyPartTransformData>();

    public SnakeBodyPartTransformData oldPosition => transformHistory.Last();

    public void SaveTransform()
    {
        transformHistory.Insert(0, GetCurrentTransformData());

        var savedLast = false;
        foreach (var transformData in transformHistory.ToArray())
        {
            var distance = Vector3.Distance(transformData.position, transform.position);
            if (distance > positionSaveDistance)
            {
                if (!savedLast)
                {
                    transformData.position = Vector3.Lerp(transform.position, 
                        transformData.position, positionSaveDistance / distance);
                    savedLast = true;
                    continue;
                }

                transformHistory.Remove(transformData);
            }
        }
    }

    private SnakeBodyPartTransformData GetCurrentTransformData()
    {
        return new SnakeBodyPartTransformData { position = transform.position, rotation = art.rotation };
    }
}
