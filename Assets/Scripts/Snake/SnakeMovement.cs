using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEditor.PackageManager;
using UnityEngine;

public class SnakeMovement : MonoBehaviour
{
    [Header("Main settings")]
    public SnakeBodyPart headBodyPart;
    public float moveSpeed;

    [Header("Ground snapping settings")]
    public LayerMask groundMask;
    public float groundDistance, positionSmoothFactor, groundRotationSmoothFactor, moveRotationSmoothFactor;

    [Header("Runtime")]
    public Vector2 moveDirection;

    public Vector3 targetPosition { get; private set; }
    public Vector3 targetUp { get; private set; }
    public Vector3 previousPosition => headBodyPart.transformHistory.First().position;

    private void FixedUpdate()
    {
        moveDirection = moveDirection.normalized;

        var forward = Vector3.Cross(headBodyPart.transform.right, targetUp);

        headBodyPart.transform.position += moveSpeed * Time.fixedDeltaTime * 
            (moveDirection.x * headBodyPart.transform.right + moveDirection.y * forward);

        transform.position = Vector3.Lerp(transform.position,
            targetPosition, positionSmoothFactor * Time.fixedDeltaTime);

        var up = Vector3.Slerp(transform.up, targetUp, groundRotationSmoothFactor * Time.fixedDeltaTime);
        transform.rotation = Quaternion.LookRotation(Vector3.Cross(transform.right, up), up);

        var delta = transform.position - previousPosition;
        if (delta.magnitude > 0.001f)
        {
            headBodyPart.art.rotation = Quaternion.Slerp(headBodyPart.art.rotation,
                Quaternion.LookRotation(transform.position - previousPosition, transform.up),
                moveRotationSmoothFactor * Time.fixedDeltaTime);
        }

        if (Physics.Raycast(transform.position + transform.up * groundDistance, -transform.up,
            out var hitInfo, float.MaxValue, groundMask))
        {
            targetPosition = hitInfo.point + hitInfo.normal * groundDistance;
            targetUp = hitInfo.normal;
        }
    }
}
