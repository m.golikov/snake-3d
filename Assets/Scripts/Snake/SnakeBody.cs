using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class SnakeBody : MonoBehaviour
{
    private const float LookRotationEpsilon = 0.01f;

    [Header("Main settings")]
    public List<SnakeBodyPart> bodyParts;
    public float bodyPartsDistance;
    public SnakeBodyPart bodyPartPrefab;
    public int startBodyPartCount = 5;

    private void Start()
    {
        bodyParts.ForEach(InitializeBodyPart);
        for (var i = 0; i < startBodyPartCount; i++)
        {
            AddBodyPart(bodyPartPrefab);
        }
    }

    private void FixedUpdate()
    {
        for (var i = 0; i < bodyParts.Count - 1; i++)
        {
            var previousPart = bodyParts[i];
            var currentPart = bodyParts[i + 1];

            currentPart.transform.position = previousPart.oldPosition.position;
            currentPart.art.rotation = previousPart.oldPosition.rotation;
        }

        bodyParts.ForEach(part =>
        {
            var delta = part.transform.position - part.transformHistory.First().position;
            if (delta.magnitude > LookRotationEpsilon)
            {
                part.art.rotation = Quaternion.LookRotation(delta, part.art.up);
            }
            part.SaveTransform();
        });
    }

    public void AddBodyPart(SnakeBodyPart bodyPartSource)
    {
        var bodyPart = Instantiate(bodyPartSource, transform);
        bodyPart.gameObject.name = bodyPartSource.name;
        InitializeBodyPart(bodyPart);
        bodyParts.Add(bodyPart);
    }

    private void InitializeBodyPart(SnakeBodyPart bodyPart)
    {
        bodyPart.positionSaveDistance = bodyPartsDistance;
        bodyPart.transform.position = bodyParts.Last().transform.position;
        bodyPart.art.rotation = bodyParts.Last().art.rotation;
        bodyPart.SaveTransform();
    }
}
