using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SnakeEater : MonoBehaviour
{
    public float attractionForce, foodEatDistance;
    public SnakeBody snakeBody;

    private void OnTriggerStay(Collider other)
    {
        var food = other.GetComponent<Food>();
        if (food)
        {
            food.transform.position += attractionForce * Time.fixedDeltaTime *
                (transform.position - food.transform.position).normalized;

            if (Vector3.Distance(food.transform.position, transform.position) < foodEatDistance)
            {
                for (var i = 0; i < food.foodValue; i++)
                {
                    snakeBody.AddBodyPart(snakeBody.bodyParts.Last());
                }

                food.Destroy();
            }
        }
    }
}
